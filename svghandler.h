#ifndef SVGHANDLER_H
#define SVGHANDLER_H

#include <QString>
#include <QLineF>
#include <QList>
#include <QPolygonF>
#include "nanosvg.h"

class SVGHandler
{
public:
    SVGHandler(const QString &fileName = QString());
    void loadFile(const QString &fileName);
    void clear();
    QList<QPolygonF> getPath();
private:
    QList<QPolygonF> path;
};

#endif // SVGHANDLER_H
