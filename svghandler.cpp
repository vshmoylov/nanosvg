#include "svghandler.h"

SVGHandler::SVGHandler(const QString &fileName)
{
    if (!fileName.isEmpty()) loadFile(fileName);
}

void SVGHandler::loadFile(const QString &fileName)
{
    clear();
    struct SVGPath* plist;
    plist = svgParseFromFile(fileName.toLocal8Bit().data()); //! TODO from QFILE

    for (SVGPath* it = plist; it; it = it->next){
        QPolygonF poly;
        for (int i = 0; i < it->npts; ++i)
//            path.append(QLineF(QPointF(it->pts[i*2], it->pts[i*2+1]),
//                                   QPointF(it->pts[(i+1)*2], it->pts[(i+1)*2+1])));
            poly.append(QPointF(it->pts[i*2], it->pts[i*2+1]));
        if (it->closed && poly.size()>1){
            poly.append(poly.first());
            //path.append(QLineF(path.first().p1(),path.last().p2()));
        }
        path.append(poly);
    }

    svgDelete(plist);
}

void SVGHandler::clear()
{
    path.clear();
}

QList<QPolygonF> SVGHandler::getPath()
{
    return path;
}
